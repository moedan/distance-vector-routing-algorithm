package mst;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;
import java.util.ArrayList;


public class DistanceVector {
	private int routerCount = 0;
	private int originalTable[][];
	private int finalTable[][];
	private int distance[];
	private int predecessor[];
	
	
	//load the input file and initial the original table
    public void loadFile(String file)
    {
    	String inputfile=file;
    	FileReader fr; 
    	BufferedReader br;
    	
	    try 
	    {
	    	String line;
	    	fr = new FileReader(inputfile);
	        br = new BufferedReader(fr);
	        line = br.readLine();
		    String[] inputs = line.split(" ");
		    routerCount = inputs.length;
	    	originalTable = new int[routerCount+1][routerCount+1];
	    	
	        //first line of original table
		    System.out.println(line);
		    for (int i = 1; i <= routerCount; i++)
		    {
		    	originalTable[1][i] = Integer.parseInt(inputs[i-1]);
		    }
	      
		    //second to last line of original table
		    for (int i = 2; i <= routerCount; i++)
		    {
		    	line = br.readLine();
		    	inputs = line.split(" ");
		    	System.out.println(line);
		    	
		    	//record the cost into an array
		    	for (int j = 1; j <= routerCount; j++)
		    	{
		    		originalTable[i][j] = Integer.parseInt(inputs[j-1]);
		    	}
		    }
	      
		    System.out.println();
		   
		    br.close();
		    fr.close();
	    } 
		catch(FileNotFoundException e) 
		{     
	        e.printStackTrace();
	    } 
		catch(IOException e)
	    {
	        e.printStackTrace();
	    }
    }
    
    //initialize distance and predecessor
    public void initialize()
    {
    	distance = new int[routerCount+1];
    	predecessor = new int[routerCount+1];
    	for (int i = 1; i <= routerCount; i++)
    	{
    		distance[i] = 100000; // a very big number to represent infinity
    		predecessor[i] = 0;
    	}
    }
    
    //find the minimal distance between src and all other nodes
    public void findDistance(int src)
    {
        distance[src] = 0;
        for(int i = 1; i <= routerCount - 1; i++)
        {
            for(int u = 1; u <= routerCount; u++)
            {
                for(int v = 1; v <= routerCount; v++)
                {
                    if(originalTable[u][v] != -1) //if u and v are neighbors
                    {
                        if(distance[v] > distance[u] + originalTable[u][v])
                        {
                            distance[v] = distance[u] + originalTable[u][v];
                            predecessor[v] = u;
                        }
                    }
                }
            }
        }
    }
    
    //compute the final routing table 
    public void computeTable()
    {	
    	finalTable = new int[routerCount+1][routerCount+1];
    	
    	//compute minimal cost for all routers
    	for(int i = 1; i <= routerCount; i++)
    	{
    		//clear distance and predecessor
    		initialize();
    		
    		//calculate the distance
    		findDistance(i);
    		finalTable[i] = distance;
    	}

	    //print out the final table
	    for (int i = 1; i <= routerCount; i++)
	    {
	    	for (int j = 1; j <= routerCount; j++)
	    	{
	    		System.out.print(" " + finalTable[i][j]);
	    	}
	    	System.out.println();
	    }
    }
   
    public char getChar(int value){
    		return (char)(value+64);
    }
    
    public void findShortestPath(int source, int dest)
    {
    	ArrayList<Integer> path = new ArrayList<Integer>();
    	int last = dest;
    	
		//clear distance and predecessor
		initialize();
    	findDistance(source);
    	
    	//find the shortest path
    	while(predecessor[last] != 0)
    	{
    		path.add(predecessor[last]);
    		last = predecessor[last];
    	}
    	
    	//print the shortest path:
    	System.out.print("Shortest path from " + getChar(source) + " to " + getChar(dest) +": ");
    	for (int i = path.size(); i > 0; i--)
    	{
    		System.out.print(getChar(path.get(i-1)) + "-"); 
    	}
    	System.out.print(getChar(dest));    	
    	System.out.println(";cost is " + distance[dest]);
    	System.out.println();
    }
	
	public static int getNum(String s){
		char c = s.toUpperCase().charAt(0);
		return c-64;
	}
	
    public static void main(String args[]) throws IOException
    {
    	Scanner sc = new Scanner(System.in);
    	DistanceVector distanceVector = new DistanceVector();
    	boolean running = true;
    	    	 
    	while(running)
    	{
    	    System.out.println(
    	    	   "1-LOAD FILE\n" +
  	           	   "2-COMPUTE FINAL ROUTING TABLE\n" +
  	           	   "3-OUTPUT OPTIMAL PATH AND MINIMUM COST\n"+
  	           	   "0-EXIT\n"
  	           	  );
    		
            int input = sc.nextInt();
            
    		switch(input)
    	    {
    		    case 1: 
    		        System.out.println("Please load original routing table data file:"); 
    		        String fileName = sc.next();
	    	        System.out.println("Original routing table is as follows:"); 
	    	        distanceVector.loadFile(fileName);
	    	        distanceVector.initialize();
	                break; 	         
    	        case 2:
    	        	System.out.println("Final routing table computed by the DV algorithm is:");
   		            distanceVector.computeTable();
   	                break; 	         
    	        case 3: 
    	        	System.out.println("Please input the source and the destination router number:");
    	        	String s = sc.next();
    	        	String d = sc.next();
    	        	int source = getNum(s);
    	        	int dest = getNum(d);
    	        	if (source > distanceVector.routerCount || dest > distanceVector.routerCount)
    	        	{
    	        		System.out.println("the source or destination router number is too big!");
    	        	}
    	        	else
    	        	{
          		        distanceVector.findShortestPath(source,dest);
    	        	}
      	            break;         
    	    	case 0: 
	    		    running = false;
	    	        break;
	    	    default:
	    	    	System.out.println("Wrong input!");	    	          	    	  
    	    }    
    		
     		System.out.println("");   		 
    	}   	
    }
}
