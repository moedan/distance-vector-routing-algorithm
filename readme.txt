How to compile and run:
Before start, make sure you have java's jdk installed.

1.Put DistanceVector.java and testfile.txt into one folder. For example, they are put in E:\project
2.From the Command Prompt, navigate to the directory: E:\project
3.From the Command Prompt, type the "javac DistanceVector.java" command to compile DistanceVector.java
4.From the Command Prompt, type the "java -cp. DistanceVector" command to run
5.The main menu will be print, and type corresponding number to run the menu

Another way to compile and run is to use IDE, such as Eclipse
1. Setup an Eclipse project, and DistanceVector.java to this project
2. Put the testfile.txt into the project folder.
3. Use the "run" botton to run DistanceVector.java
4. Follow the menu